﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakablePlatform : MonoBehaviour
{
    public GameObject player;

    private float vel;
    
    void Start()
    {
        
    }

    void Update()
    {
        vel = player.GetComponent<Rigidbody>().velocity.magnitude;
        //Debug.Log(playerVel); 
    }

    private void OnCollision(Collider other)
    {
        if(vel>=8f)
            {
                //shatter
                //To implement multiple stages of breakage, change the platform's material and flip a boolean
                GetComponent<Collider>().enabled = false;
            }
    }
}
