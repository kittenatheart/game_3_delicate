﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ending : MonoBehaviour
{
    public GameObject player;
    public GameObject winTextObject;
    float timer=0;
    bool timerReached=true;

    void Start()
    {
        winTextObject.SetActive(false);
    }

    void Update()
    {
        if(!timerReached){
            timer+=Time.deltaTime;
        }
        if(!timerReached&&timer>5){
            timerReached=true;
            SceneManager.LoadScene("Overworld");
        }

        if(Input.GetKey("escape")){
            Application.Quit();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            winTextObject.SetActive(true);
            timerReached=false;
            player.SetActive(false);
        }
    }
}
