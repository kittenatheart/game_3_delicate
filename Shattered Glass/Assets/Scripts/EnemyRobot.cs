﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRobot : MonoBehaviour
{
    public float speed = 0;
    public GameObject playerObject;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        transform.LookAt(playerObject.transform);
        transform.position += transform.forward * Time.deltaTime * speed;
    }
}
