﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollisions : MonoBehaviour
{
    public GameObject robot;

    void Start()
    {
        robot.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Abyss Door"))
        {
            SceneManager.LoadScene("Abyss");
        }
        if (other.CompareTag("Ending"))
        {
            robot.SetActive(false);
        }
        if (other.CompareTag("Robot Spawn"))
        {
            robot.SetActive(true);
        }
        if (other.CompareTag("Robot"))
        {
            SceneManager.LoadScene("Abyss");
        }
    }
}
