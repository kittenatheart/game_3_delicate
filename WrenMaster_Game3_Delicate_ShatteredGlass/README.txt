What was the goal, intent, intended tone, or metaphor of your prototype? Why did you make it?
The metaphor of the game was "breaking to bounds of reality" combined with a theme of platforming on breakable platforms. 
It was meant to be a sort of meta-commentary on the breakability of games, akin to The Stanley Parable.

How did the theme inspire this game?
Both the breakable platforms and the breakable reality are decilate.

What are ALL the controls? 
Mouse to look around, WASD to move, space to jump, Esc to quit.